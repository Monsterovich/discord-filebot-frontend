sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("discord.filebot.frontend.controller.App", {
		onInit: function() {
			document.title = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("appTitle");
		}
	});

});
