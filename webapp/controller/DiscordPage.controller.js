sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/format/DateFormat",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/m/MessageToast"
], function(Controller, DateFormat, JSONModel, MessageBox, MessageToast) {
	"use strict";

	return Controller.extend("discord.filebot.frontend.controller.App", {
		onInit: function () {
			this.oView = this.getView();
			this.oResouceBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();

			const sModelPath = jQuery.sap.getModulePath("discord.filebot.frontend.model.DiscordPage", ".json");
			this.oView.setModel(new JSONModel(sModelPath), "view");

			this.fileUploadForm = this.oView.byId("fileUploadForm");
			this.filesTable = this.oView.byId("filesTable");
			this.fileUploader = this.oView.byId("fileUploader");
			const oConfigModel = this.getOwnerComponent().getModel("config");
			oConfigModel.pSequentialImportCompleted.then(() => {
				this.fileUploader.setUploadUrl(this.getBackendUrl("/submit", oConfigModel));
			});
			this.authDialog = this.oView.byId("authDialog");
			this.authRequestErrorMessageStrip = this.oView.byId("authRequestErrorMessageStrip");

			// this.getOwnerComponent().getRouter().attachRouteMatched("DiscordPage", this.onRouteMatched, this);
		},

		// onRouteMatched: function (oEvent) {

		// },

		millisecondsToDate: function (iMillis) {
			const oDateFormat = DateFormat.getDateInstance({
				pattern: "EEE, MMM d, yyyy"
			});
			
			return oDateFormat.format(new Date(iMillis)); 
		},

		openAuthDialog: function (oEvent) {
			this.authDialog.open();
		},

		onDialogClose: function (oEvent) {
			oEvent.getSource().getParent().close();
		},

		onSubmitAuthRequestPress: function (oEvent) {
			const oPayload = this.oView.getModel("view").getProperty("/authDialog/form");

			this.authDialog.setBusy(true);

			this.signIn({
				payload: oPayload,
				success: () => {
					this.authRequestErrorMessageStrip.setVisible(false);
					this.authRequestErrorMessageStrip.setText();

					this.authDialog.setBusy(false);
					this.authDialog.close();
				},
				error: (oError) => {
					const oErrorData = JSON.parse(oError.responseText);

					let sErrorText = null;
					switch (oErrorData.errorCode) {
						case 0:
							sErrorText = this.oResouceBundle.getText("messageStrip.userDoesntExist");
							break;
						default:
							break;
					}

					this.authRequestErrorMessageStrip.setText(sErrorText);
					this.authRequestErrorMessageStrip.setVisible(true);

					this.authDialog.setBusy(false);
				}
			});
		},

		signIn: function (oParameters) {
			const oLoginModel = this.oView.getModel("login");

			oLoginModel.loadData(this.getBackendUrl("/auth"), oParameters.payload, true, "POST").then(() => {
				const oNotifyModel = this.oView.getModel("notify");

				const oPayload = {
					user_id: oLoginModel.getProperty("/id")
				};

				const aPromises = [
					oNotifyModel.loadData(this.getBackendUrl("/getnotify"), oPayload, true),
					this.refreshFiles()
				];

				Promise.all(aPromises).then(() => {
					oParameters.success();
				});
			}).catch((oError) => {
				oParameters.error(oError);
			});
		},

		signOut: function (oEvent) {
			MessageBox.confirm(this.oResouceBundle.getText("messageBox.signOutConfirmation"), {
				onClose: (sAction) => {
					if (sAction === MessageBox.Action.OK) {
						const aModels = [
							this.oView.getModel("login"),
							this.oView.getModel("notify"),
							this.oView.getModel("files")
						];
			
						aModels.forEach(oModel => {
							oModel.setData({});
						});
					}
				},
			});
		},

		onFileUploadStart: function (oEvent) {
			const oSource = oEvent.getSource();
			const encodedFileName = encodeURIComponent(oSource.getValue());

			oSource.removeAllParameters();

			const aParameters = [
				new sap.ui.unified.FileUploaderParameter({
					name: "file_name",
					value: encodedFileName
				}),
				new sap.ui.unified.FileUploaderParameter({
					name: "guild_label",
					value: this.oView.getModel("view").getProperty("/selectedGuildLabel")
				}),
				new sap.ui.unified.FileUploaderParameter({
					name: "user_id",
					value: this.oView.getModel("login").getProperty("/id")
				})
			];

			aParameters.forEach((oParameter) => {
				oSource.addParameter(oParameter);
			});

			oSource.removeAllHeaderParameters();

			const aHeaderParameters = [
				new sap.ui.unified.FileUploaderParameter({
					name: "X-Requested-With",
					value: "XMLHttpRequest"
				}),
				new sap.ui.unified.FileUploaderParameter({
					name: "Access-Control-Allow-Origin",
					value: "*"
				})
			];

			aHeaderParameters.forEach((oHeaderParameter) => {
				oSource.addHeaderParameter(oHeaderParameter);
			});

			this.fileUploadForm.setBusy(true);
		},

		onFileUploadSubmitPress: function (oEvent) {
			this.fileUploader.upload();
		},

		onFileDelete: function (oEvent) {
			const sFilename = oEvent.getSource().getBindingContext("files").getProperty("filename");
			const oPayload = {
				user_id: this.oView.getModel("login").getProperty("/id"),
				file_name: sFilename
			};

			new JSONModel().loadData(this.getBackendUrl("/deletefile"), oPayload, true, "POST").then(() => {
				MessageToast.show(this.oResouceBundle.getText("messageToast.fileDeleted", [sFilename]));
			}).catch(() => {
				MessageToast.show(this.oResouceBundle.getText("messageToast.fileDeletionError", [sFilename]));
			}).finally(() => {
				this.refreshFiles();
			});
		},

		onFileUploadComplete: function (oEvent) {
			const oResponseData = JSON.parse(oEvent.getParameter("responseRaw"));
			MessageToast.show(this.oResouceBundle.getText("messageToast.fileUploaded", [oResponseData.filename]));

			this.fileUploader.clear();
			this.fileUploadForm.setBusy(false);
			this.refreshFiles();
		},

		refreshFiles: function () {
			const oPayload = {
				user_id: this.oView.getModel("login").getProperty("/id")
			};

			this.filesTable.setBusy(true);
			const pRefresh = this.oView.getModel("files").loadData(this.getBackendUrl("/getfiles"), oPayload, true);
			pRefresh.finally(() => {
				this.filesTable.setBusy(false);
			});
			return pRefresh;
		},

		getBackendUrl: function (sRequest, oConfigModel) {
			if (!oConfigModel) {
				oConfigModel = this.oView.getModel("config");
			}

			const oRegExp = new RegExp(/^(https?:\/\/)?(www\.)?([^/]+)/gm);
			const aAddress = document.URL.match(oRegExp);
			let sBackendUrl = null;

			if (aAddress?.length) {
				if (aAddress[0].includes("192.168") || aAddress[0].includes("localhost") || aAddress[0].includes("127.0.0.1")) {
					sBackendUrl = oConfigModel.getProperty("/backendUrlLocal");
				} else {
					sBackendUrl = oConfigModel.getProperty("/backendUrl");
				}
			}

			return `${sBackendUrl}${sRequest}`;
		}
	});

});
